
function problemaUno() {
    console.log('----------------------------- --------------');
    console.log('Problema 1');
let anObject = {
    foo: 'bar',
    length: 'interesante',
    '0': 'zero!',
    '1': 'one!'
};
let anArray = ['zero.','one.'];

console.log('Objeto: ',anObject);
console.log('Objeto: ',anArray);
console.log('Array posicion 0: ',anArray[0], anObject[0]);
console.log('Array posicion 1: ',anArray[1], anObject[1]);
console.log('Magnitudes de arreglos: ',anArray.length, anObject.length);
console.log('atributo con foo no en anArray y AnObject: ',anArray.foo, anObject.foo);
console.log();
}
function problemaDos(){
    console.log('----------------------------- --------------');
    console.log('Problema 2');
    var obj = {
        a: 'Hola',
        b: 'esto es',
        c: 'JavaScript'
    }
    console.log(Object.values(obj));
}
function problemaTres (){
    console.log('----------------------------- --------------');
    console.log('Problema 3');
    for (let contador = 0; contador < 100; contador+=2){
        console.log('Numero: '+ contador);
    }
}
function problemaCuatro (){
    console.log('----------------------------- --------------');
    console.log('Problema 4');
    let zero = 0;
    function multi(num){ return num * 2;}
    function agregar(a = 1 + zero, b = a, c = b + a, d=multi(c)){
        console.log((a + b + c), d);
    }
    agregar(1);
    agregar(3);
    agregar(2, 7);
    agregar(1, 2, 5);
    agregar(1, 2, 5, 10);
}
function problemaCinco(){
    console.log('----------------------------- --------------');
    console.log('Problema 5');
    class MyClass {
        constructor(){
            this.names_ = [];
        }
        set name(value) {
        this.names_.push(value);
        }
        get name(){
            return this.names_[this.names_.length - 1];
        }
    }
    const MyClassIntance = new MyClass();
    MyClassIntance.name = 'Joe';
    MyClassIntance.name = 'Bob';
    console.log(MyClassIntance.name);
    console.log(MyClassIntance.names_);
}
function problemaSeis(){
    console.log('----------------------------- --------------');
    console.log('Problema 6');
    const classIntance = new class {
        get prop() {
            return 5;
        }
        
    };
    classIntance.prop = 10;
    console.log(classIntance.prop);
}
function problemaSiete(){
    console.log('----------------------------- --------------');
    console.log('Problema 7');
    class Queue {
        constructor (){
            const list = [];
            this.enqueue = function(type){
                list.push(type);
                return list.shift();
            };
            this.dequeue = function () {
                return list.shift();
            };
        }
    }
    var q = new Queue;
    q.enqueue(9); 
    q.enqueue(8); 
    q.enqueue(7);
    console.log(q.dequeue()); 
    console.log(q.dequeue()); 
    console.log(q.dequeue()); 
    console.log(q); 
    console.log(Object.keys(q)); 
}
function problemaOcho (){
    console.log('----------------------------- --------------');
    console.log('Problema 8');
    class Persona {
        firstname = '';
        lastname = '';

    set firstname(value) {
        if (value != '') {
            value = '';
        }
        this.firstname = value;
    }
    get firstname() {
        return this.firstname;
    }

    set lastname(value) {
        if (value != '') {
            value = '';
        }
        this.lastname = value;
    }
    get lastname() {
        return this.lastname;
    }
    }
    let persona = new Persona('John', 'Doe');
    persona.firstname = 'foo';
    persona.lastname = 'Bar';
    console.log(persona.firstname, persona.lastname);
}
    function muestraMensaje1() {
    let deleteBtn = document.querySelectorAll('[data-deletepost]');

        let resp = "";

        let eliminar = alert("Deseas eliminar el elemento");
        if (eliminar == true) {
            resp
        } else {
            resp
        }
        document.getElementById("post-102").innerHTML = resp;
    }
    function muestraMensaje2() {
    let deleteBtn = document.querySelectorAll('[data-deletepost]');

        let resp = "";
        let eliminar = alert("Deseas eliminar el elemento");

        if (eliminar == true) {
            resp
        } else {
            resp
        }
        document.getElementById("post-103").innerHTML = resp;
    }

problemaUno();
problemaDos(); 
problemaTres();
problemaCuatro();
problemaCinco();
problemaSeis();
problemaSiete();
problemaOcho();