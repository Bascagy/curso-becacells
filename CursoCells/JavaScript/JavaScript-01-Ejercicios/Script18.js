console.log('--------------------- ---------');
console.log('Problema 18');

function foo() {
    var a = 'hello';

    function bar(){
        var b = 'world';
        console.log(a);
        console.log(b);
    }
    console.log(a);
    console.log(b);   
}
console.log(a);
console.log(b);

foo();