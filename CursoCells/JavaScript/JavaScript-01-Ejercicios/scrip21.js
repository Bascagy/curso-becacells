console.log('--------------------- ---------');
console.log('Problema 21');
var array = ['a','b','c'];
console.log(array);
console.log(array.join('->'));
console.log(array.join('.'));
console.log('a,b,c'.split(''));
console.log('5.4.3.2.1'.split('.'));

console.log('--------------------- ---------');
console.log('Problema 22');
var arrays = [5,10,15,20,25];
Array.isArray(arrays);

var val=arrays.includes(5);
console.log(val);
var val2 = arrays.includes(5,0);
console.log(val2);
var val3 = arrays.indexOf(25);
console.log(val3);
val4 = arrays.lastIndexOf(5,10);
console.log(val4);

console.log('--------------------- ---------');
console.log('Problema 23');
var arrays2 = ['a','b', 'c', 'd', 'e', 'f'];
console.log(arrays2);
var valor4 = arrays2.copyWithin(5, 0, );
console.log(valor4);
var valor5 = arrays2.copyWithin(3, 0,2);
console.log(valor5)
var valor6 = arrays2.fill('z', 0, 1);
console.log(valor6);

console.log('--------------------- ---------');
console.log('Problema 24');
var arraysNombres= ['Alberto', 'Ana', 'Mauricio', 'Bernardo', 'Zoe'];
console.log(arraysNombres);
var sorts = arraysNombres.sort();
console.log(sorts);
var reverso =arraysNombres.reverse();
console.log(reverso);